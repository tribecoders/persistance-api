# Query DSL
While writing string queries similar to SQL as string-based JPQL, is easy and siple. It poses some drawbacks in more complicated cases. The main one is no type safty when writing queries. This can lead to errors and issues that occur during rintime and could be avoided. Also queries that base on real time values or would change depanding on evaluated conditions may require some string concatenation that is error prone.

To avoid above pitfalls we can add Query DSL library on top of JPA. During the build process, Q-types (Query classes) will be generated for each of defined entities. This way for our Book, QBook class will be generated. This classes can afterwards be used to write queries.

## Predicates
Like with JPA or with connection with JPA we can create QueryDslPredicateExecutor repository.

```
public interface BookJpaRepository extends JpaRepository<Book, UUID>, QueryDslPredicateExecutor<Book> 
```

This will extend our with another set of usefull functions. One of them is **findAll** function that takes Prediate as a parameter. Predicate in this case can be treated as a **WHERE** filter that is applied to all result entities.

```
BooleanBuilder builder = new BooleanBuilder();
Predicate predicate = builder.and(QBook.book.author.id.eq(author.getId())).getValue();
bookJpaRepository.findAll(predicate);
```

In this whay we can define rich queries that can be compiled and verified before system is executed.

## Custom queries
As with JPA we are not bound to repository defined functions. We can create whole query from scratch.

```
JPAQuery<QBookDetails> query = new JPAQuery(entityManager);
```

Now we just need to define end execute the query.

```
query.select(new QBookDetails(book, chapter.length.sum().as(length)));
query.from(book);
query.join(book.chapters, chapter);
if (longerThan != null) {
  query.where(length.goe(longerThan));
}
query.groupBy(book);
query.orderBy(length.asc(), book.title.asc());
return query.createQuery().getResultList();
```

To return custom class we had to mark its constructor with **@QueryProjection** decorator so Q-type class would be generated for it.

As you can see, we can use all known from SQL structures, but this time with type checking and code completion. 

