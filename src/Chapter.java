package com.touramigo.service.tourdata.repository;

import com.touramigo.service.tourdata.entity.db.Tour;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "book_chapter")
public class Chapter {

    @Id
    private UUID id;

    private String title;

    private String length;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    private Book book;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
