package com.touramigo.service.tourdata.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Visitor;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.jpa.impl.JPAQuery;
import com.touramigo.service.tourdata.entity.SortOrder;
import com.touramigo.service.tourdata.entity.db.*;
import com.touramigo.service.tourdata.rest.SearchResult;
import com.touramigo.service.tourdata.rest.TourSearchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookQueryDslRepository {
    @Autowired
    BookJpaRepository bookJpaRepository;

    @PersistenceContext
    EntityManager entityManager;

    public List<Book> findByAuthor(Author author) {
        BooleanBuilder builder = new BooleanBuilder();
        Predicate predicate = builder.and(QBook.book.author.id.eq(author.getId())).getValue();
        List<Book> books = new ArrayList<Book>();
        bookJpaRepository.findAll(predicate).iterator().forEachRemaining(books::add);
        return books;
    }

    public List<QBookDetails> findByLength(Integer longerThan) {

        QBook book = QBook.book;
        QChapter chapter = QChapter.chapter;

        NumberPath<Integer> length = Expressions.numberPath(Integer.class, "length");

        JPAQuery<QBookDetails> query = new JPAQuery(entityManager);
        query.select(new QBookDetails(book, chapter.length.sum().as(length)));
        query.from(book);
        query.join(book.chapters, chapter);
        if (longerThan != null) {
          query.where(length.goe(longerThan));
        }
        query.groupBy(book);
        query.orderBy(length.asc(), book.title.asc());
        return query.createQuery().getResultList();
    }
}
