package com.touramigo.service.tourdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface BookJpaRepository extends JpaRepository<Book, UUID>, QueryDslPredicateExecutor<Book> {

    @Query("select b from Book b where b.id in (:bookIds)")
    List<Book> findByIds(@Param("tourIds") List<UUID> bookIds);

    @Query("select b from Book b where b.author.id = :authorId")
    List<Book> findByAuthor(@Param("authorId") UUID authorId);

    @Query("select distinct b from Book b join b.chapter tch where tch.length > :length")
    List<Book> findBookLongerThan(@Param("length") Integer length);
}
