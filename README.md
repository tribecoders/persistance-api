# Object-Relational Mapping
In this tutorial you will learn various approaches to get your data from data base using a Java Persistance API (JPA). We will run custom queries and use Query DSL to find data we need.

## Data model
Before any data will be fetched, we need to define our data strucure. We will model a simple book collection. We need to define entity objects that will represent our database structure. 

- Book
- Author
- Chapter

We will define simple classes for each of our data tables and mark them with JPA decorators

```
@Entity
@Table
```

## Book
This is core entity that we will query about. Apart from basic information, it contains:

- author -  many to one relation: book has one author, author can have multiple books

- chapters - one to many relation: book has many chapters, chapter belong to single book 