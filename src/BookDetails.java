package com.touramigo.service.tourdata.repository;

import com.querydsl.core.annotations.QueryProjection;

public class BookDetails {
    private Book book;
    private Integer pages;

    @QueryProjection
    BookDetails(Book book, Integer pages) {
        this.book = book;
        this.pages = pages;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
