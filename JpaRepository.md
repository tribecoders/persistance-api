# Java Persistence API
It is standard ORM (Object-Relational Mapping) for Java. It maps relational database tables and relations to entities (Java Objects). Translating code to database queries. It also defines query language that is similar to SQL that can be used for more sophisticated data operations.

## Repository
JPA repository can be defined as a interface that extend JpaRepository. In most of cases you will define one ropository for one of defined entities. You can provide tentity definition when defining repository.

```
public interface BookJpaRepository extends JpaRepository<Book, UUID>
```

In this case we define a repository for a **Book** entity with primary key of type **UUID**. This will define basic interface functions that are required for data manipulation such as

* findOne(UUID id) - that translates to select single row from book table
* findAll - that translates to select all data from book table
* save(Book book) - that will add new book row based on book entity 
* update(UUID id, Book book) - that will update book row based on book entity
* delete(UUID id) - that translated to delete single row from book table

and many others

## Custom queries
Sometimes simple queries are not enough. Lucly we can define additional repository functions and define custom query with **@Query*** decorator. In our example we defined couple more complicated select queries.

*findByIds
*findByAuthor
*findLongerThan

```
@Query("select b from Book b where b.id in (:bookIds)")
List<Book> findByIds(@Param("tourIds") List<UUID> bookIds);
```

As you can see query language is fairly simmila to normal SQL we just use entities in place of tables and fields and can use parameter that will be passed to the function we define. In this example we select or Book entities that have their ids in the list that was passed as parameter.

```
@Query("select b from Book b where b.author.id = :authorId")
List<Book> findByAuthor(@Param("authorId") UUID authorId);
```

We can use any other entity that we defined relation with. This can be done with a dot notation that is used when accessing object properties. 

Other expressions known from SQL are also available, the next example use **join** concept. 

```
@Query("select distinct b from Book b join b.chapter tch where tch.length > :length")
List<Book> findLongerThan(@Param("length") Integer length);
```

We show here only custom queries examples, but you can also do some custom insert, update and delete to. 

